#include "Main.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <../../external/GLM/glm/gtx/rotate_vector.hpp>
#undef GLM_ENABLE_EXPERIMENTAL

static float  kTreeSegmentHeightRatio = 0.65f;
static float  kTreeSegmentWidthRatio = 0.5f;
static size_t kTreeTrunkBottomVertices = 64;
static size_t kTreeDepth = 5;

TreeMeshGenerator::TreeMeshGenerator(glm::vec3 trunkStart, glm::vec3 trunkEnd, float r)
{
    generateSegment(trunkStart, trunkEnd, r, kTreeTrunkBottomVertices, 0);
    m_bottomVertices.clear();
    m_bottomNormals.clear();
    m_topVertices.clear();
    m_topNormals.clear();
}

MeshPtr TreeMeshGenerator::getMesh()
{
    DataBufferPtr vertexBuffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    vertexBuffer->setData(m_vertices.size() * sizeof(float) * 3, m_vertices.data());

    DataBufferPtr normalBuffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    normalBuffer->setData(m_normals.size() * sizeof(float) * 3, m_normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, vertexBuffer);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, normalBuffer);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(static_cast<GLuint>(m_vertices.size()));

    return mesh;
}

void TreeMeshGenerator::generateSegmentPoints(glm::vec3 bottomPos, glm::vec3 topPos, float bottomR, int bottomVerticesCount)
{
    m_bottomVertices.clear();
    m_bottomNormals.clear();
    m_topVertices.clear();
    m_topNormals.clear();

    auto topR = bottomR * kTreeSegmentWidthRatio;
    auto topVerticesCount = bottomVerticesCount / 2;

    auto rotationMatrix = glm::orientation(glm::normalize(topPos - bottomPos), glm::vec3(0.0f, 0.0f, 1.0f));

    for (auto i = 0; i < bottomVerticesCount; ++i)
    {
        float angle = 2.0f * glm::pi<float>() * i / bottomVerticesCount;
        glm::vec4 pt(cosf(angle), sinf(angle), 0.0f, 0.0f);

        m_bottomVertices.emplace_back(glm::vec3(rotationMatrix * (pt * bottomR)) + bottomPos);
        m_bottomNormals.emplace_back(rotationMatrix * pt);

        if (i % 2 == 0)
        {
            glm::vec4 topPt = pt * topR + glm::vec4(0.0f, 0.0f, glm::distance(bottomPos, topPos), 0.0f);
            m_topVertices.emplace_back(glm::vec3(rotationMatrix * topPt) + bottomPos);
            m_topNormals.emplace_back(rotationMatrix * pt);
        }
    }
}


void TreeMeshGenerator::generateBottomTriangle(size_t bottomIdx1, size_t bottomIdx2, size_t topIdx)
{
    size_t bottomVerticesCount = m_bottomVertices.size();
    size_t topVerticesCount = m_topVertices.size();

    m_vertices.push_back(m_bottomVertices[bottomIdx1 % bottomVerticesCount]);
    m_vertices.push_back(m_bottomVertices[bottomIdx2 % bottomVerticesCount]);
    m_vertices.push_back(m_topVertices[topIdx % topVerticesCount]);
    m_normals.push_back(m_bottomNormals[bottomIdx1 % bottomVerticesCount]);
    m_normals.push_back(m_bottomNormals[bottomIdx2 % bottomVerticesCount]);
    m_normals.push_back(m_topNormals[topIdx % topVerticesCount]);
}

void TreeMeshGenerator::generateTopTriangle(size_t bottomIdx, size_t topIdx1, size_t topIdx2)
{
    size_t bottomVerticesCount = m_bottomVertices.size();
    size_t topVerticesCount = m_topVertices.size();

    m_vertices.push_back(m_bottomVertices[bottomIdx % bottomVerticesCount]);
    m_vertices.push_back(m_topVertices[topIdx1 % topVerticesCount]);
    m_vertices.push_back(m_topVertices[topIdx2 % topVerticesCount]);
    m_normals.push_back(m_bottomNormals[bottomIdx % bottomVerticesCount]);
    m_normals.push_back(m_topNormals[topIdx1 % topVerticesCount]);
    m_normals.push_back(m_topNormals[topIdx2 % topVerticesCount]);
}

void TreeMeshGenerator::generateNextSegments(glm::vec3 bottomPos, glm::vec3 topPos, float bottomR, int bottomVerticesCount, int depth)
{
    if (depth >= kTreeDepth)
    {
        return;
    }

    auto rotationMatrix = glm::orientation(glm::normalize(topPos - bottomPos), glm::vec3(0.0f, 0.0f, 1.0f));

    std::vector<glm::vec4> directionProjections;
    directionProjections.emplace_back(1.0f, 0.0f, 0.0f, 0.0f);
    directionProjections.emplace_back(-1.0f, 0.0f, 0.0f, 0.0f);
    directionProjections.emplace_back(0.0f, 1.0f, 0.0f, 0.0f);
    directionProjections.emplace_back(0.0f, -1.0f, 0.0f, 0.0f);

    auto segmentLength = glm::distance(topPos, bottomPos);

    for (auto&& directionProjection : directionProjections)
    {
        auto direction = glm::normalize(topPos - bottomPos + glm::vec3(rotationMatrix * directionProjection) * segmentLength);
        auto nextTopPos = topPos + direction * segmentLength * kTreeSegmentHeightRatio;

        generateSegment(topPos, nextTopPos, bottomR * kTreeSegmentWidthRatio, bottomVerticesCount / 2, depth + 1);
    }
}

void TreeMeshGenerator::generateSegment(glm::vec3 bottomPos, glm::vec3 topPos, float bottomR, int bottomVerticesCount, int depth)
{
    generateSegmentPoints(bottomPos, topPos, bottomR, bottomVerticesCount);

    for (size_t i = 0; i * 2 < bottomVerticesCount; ++i)
    {
        generateBottomTriangle(2 * i + 1, 2 * i + 2, i + 1);
        generateTopTriangle(2 * i + 1, i + 1, i);
        generateBottomTriangle(2 * i + 2, 2 * i + 3, i + 1);
    }

    generateNextSegments(bottomPos, topPos, bottomR, bottomVerticesCount, depth);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void TreeApplication::makeScene()
{
    Application::makeScene();

    getCameraMover() = std::make_shared<FreeCameraMover>();

    m_tree = TreeMeshGenerator(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), 0.125f).getMesh();
    m_tree->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-1.0f, -1.0f, -0.5f)));
    //Создаем шейдерную программу
    m_shader = std::make_shared<ShaderProgram>("692StrenadkoData1/shaderNormal.vert", "692StrenadkoData1/shader.frag");
}

void TreeApplication::draw()
{
    Application::draw();

    int width, height;
    glfwGetFramebufferSize(getWindow(), &width, &height);

    glViewport(0, 0, width, height);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Устанавливаем шейдер.
    m_shader->use();

    //Устанавливаем общие юниформ-переменные
    m_shader->setMat4Uniform("viewMatrix", getCamera().viewMatrix);
    m_shader->setMat4Uniform("projectionMatrix", getCamera().projMatrix);

    //Рисуем первый меш
    m_shader->setMat4Uniform("modelMatrix", m_tree->modelMatrix());
    m_tree->draw();
}

int main()
{
    TreeApplication app;
    app.start();

    return 0;
}
//
// Created by vika on 06.03.2020.
//

