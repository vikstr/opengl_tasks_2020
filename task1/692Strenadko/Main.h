#pragma once

#include <vector>

#include "Application.hpp"
#include "ShaderProgram.hpp"
#include "Mesh.hpp"

class TreeMeshGenerator
{
public:
    TreeMeshGenerator(glm::vec3 trunkStart, glm::vec3 trunkEnd, float r);

public:
    MeshPtr getMesh();

private:
    void generateSegmentPoints(glm::vec3 bottomPos, glm::vec3 topPos, float bottomR, int bottomVerticesCount);
    void generateBottomTriangle(size_t bottomIdx1, size_t bottomIdx2, size_t topIdx);
    void generateTopTriangle(size_t bottomIdx, size_t topIdx1, size_t topIdx2);
    void generateNextSegments(glm::vec3 bottomPos, glm::vec3 topPos, float bottomR, int bottomVerticesCount, int depth);
    void generateSegment(glm::vec3 bottomPos, glm::vec3 topPos, float bottomR, int bottomVerticesCount, int depth);

private:
    std::vector<glm::vec3> m_vertices;
    std::vector<glm::vec3> m_normals;

    std::vector<glm::vec3> m_bottomVertices, m_bottomNormals;
    std::vector<glm::vec3> m_topVertices, m_topNormals;
};

class TreeApplication : public Application
{
public:
    void makeScene() override;
    void draw() override;

    std::shared_ptr<CameraMover> getCameraMover() { return _cameraMover; }
    CameraInfo getCamera()     { return _camera;      }
    GLFWwindow* getWindow()      { return _window;      }
private:
    MeshPtr m_tree;
    ShaderProgramPtr m_shader;
};

//
// Created by vika on 06.03.2020.
//

#ifndef OPENGL_TASKS_2020_MAIN_H
#define OPENGL_TASKS_2020_MAIN_H

#endif //OPENGL_TASKS_2020_MAIN_H
