/**
Пофрагментное освещение точечным источником света с учетом затухания. Окружающий, диффузный и бликовый свет.
*/

#version 330

struct LightInfo
{
    vec3 pos; //положение источника света в мировой системе координат (для точечного источника)
    vec3 La; //цвет и интенсивность окружающего света
    vec3 Ld; //цвет и интенсивность диффузного света
    vec3 Ls; //цвет и интенсивность бликового света
    float attenuation; //затухание
};
uniform LightInfo light;
const vec3 Ks = vec3(0.5, 0.5, 0.5);
struct MaterialInfo
{
    float shininess;
};
uniform MaterialInfo material;

in vec2 texCoord;
in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 lightPosCamSpace; //положение источника света в системе координат камеры (интерполировано между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)

out vec4 fragColor;

uniform sampler2D myTextureSampler;

void main()
{
    vec4 texColor = texture(myTextureSampler, texCoord);
    if (texColor.a == 0.0) {
        fragColor = vec4(0.0, 0.0, 0.0, 0.0);
        return;
    }
    vec3 diffuseColor = texColor.rgb;
    vec3 lightDirCamSpace = lightPosCamSpace.xyz - posCamSpace.xyz; //направление на источник света
    float distance = length(lightDirCamSpace);
    lightDirCamSpace = normalize(lightDirCamSpace); //направление на источник света

    vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции

    float attenuationCoef = 1.0 / (1.0 + light.attenuation * distance);

    float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)
    vec3 color = light.La * diffuseColor + light.Ld * diffuseColor * NdotL; //цвет вершины

    if (NdotL > 0.0)
    {
        vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
        vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

        float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
        blinnTerm = pow(blinnTerm, material.shininess); //регулируем размер блика

        color += light.Ls * Ks * blinnTerm;
    }

    fragColor = vec4(color, texColor.a);
}
