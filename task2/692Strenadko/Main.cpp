#include <../../external/GLM/glm/gtx/rotate_vector.hpp>
#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include "Texture.hpp"
#include "LightInfo.hpp"
#define GLM_FORCE_RADIANS
#include <iostream>
#include <vector>
#include <glm/gtx/fast_trigonometry.inl>
#include <glm/gtx/easing.inl>

#include "get_Mesh.h"

class SampleApplication : public Application
{
public:
    MeshPtr _tree;
    MeshPtr _leaves;
    MeshPtr _marker;

    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;

    TexturePtr _barkTexture;
    TexturePtr _leafTexture;

    GLuint _sampler{};
    GLuint _leafSampler{};

    // Угол направленного источника света
    float _lr = 5.0;
    float _phi = 3.14159265358979323846264338327950288 * 0.00f;
    float _theta = 3.14159265358979323846264338327950288 * 0.25f;

    // Параметры направленного источника света
    LightInfo _light;
    float _treeShininess{};
    float _leavesShininess{};

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();
        //Создаем меш
        std::tie(_tree, _leaves) = CreateTreeMesh(5, 3, 100);

        auto modelMatrix = glm::translate(glm::mat4(1.0), glm::vec3(-1.0f, -1.0f, -0.5f));
        _tree->setModelMatrix(modelMatrix);
        _leaves->setModelMatrix(modelMatrix);

        _treeShininess = 100.0;
        _leavesShininess = 100.0;

        _marker = makeSphere(0.1f);

        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);
        _light.attenuation0 = 1.0f;

        //Создаем шейдерную программу
        _shader = std::make_shared<ShaderProgram>("692StrenadkoData2/shaderNormal.vert", "692StrenadkoData2/shader.frag");
        _markerShader = std::make_shared<ShaderProgram>("692StrenadkoData2/marker.vert", "692StrenadkoData2/marker.frag");

        _barkTexture = loadTexture("692StrenadkoData2/Texture.jpg");
        _leafTexture = loadTexture("692StrenadkoData2/TexturesLeaves.png");

        InitSampler(_sampler);
        InitSampler(_leafSampler);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

    void update() override
    {
        Application::update();
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
                ImGui::SliderFloat("attenuation0", &_light.attenuation0, 0.0f, 10.0f);
            }
            if (ImGui::CollapsingHeader("Material")) {
                ImGui::SliderFloat("tree shininess", &_treeShininess, 0.1f, 255.0f);
                ImGui::SliderFloat("leaves shininess", &_leavesShininess, 0.1f, 255.0f);
            }
        }
        ImGui::End();
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        _shader->use();
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _marker->setModelMatrix(glm::translate(glm::mat4(1.0f), _light.position));

        //Устанавливаем шейдер.

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        _shader->setVec3Uniform("light.pos", _light.position);
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);
        _shader->setFloatUniform("light.attenuation", _light.attenuation0);

        //Рисуем первый меш
        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _barkTexture->bind();
        _shader->setMat4Uniform("modelMatrix", _tree->modelMatrix());
        _shader->setFloatUniform("material.shininess", _treeShininess);
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _tree->modelMatrix()))));
        _tree->draw();

        //Рисуем второй меш
        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _leafSampler);
        _leafTexture->bind();
        _shader->setMat4Uniform("modelMatrix", _leaves->modelMatrix());
        _shader->setFloatUniform("material.shininess", _leavesShininess);
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _leaves->modelMatrix()))));
        _leaves->draw();

        _markerShader->use();
        _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * _marker->modelMatrix());
        _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
        _marker->draw();

        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void InitSampler(GLuint& sampler) {
        glGenSamplers(1, &sampler);
        glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glSamplerParameteri(sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
//
// Created by vika on 06.03.2020.
//

