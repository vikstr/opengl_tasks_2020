#include <random>
#include "Tree.h"

State GenerateNewState(const State &state, const glm::vec3 &dir) {
    State new_state(state);

    new_state.pos += dir;
    new_state.dir = dir;
    new_state.dir *= 0.9;

    return new_state;
}

glm::vec3 Step(const State& state) {
    std::random_device r;
    std::seed_seq seed{r(), r(), r(), r(), r(), r()};
    std::mt19937_64 eng(seed);
    std::uniform_real_distribution<float> dist((float)-1, 1);

    float angle = dist(eng);
    auto ret = glm::rotateX(state.dir, angle);
    angle = dist(eng);
    ret = glm::rotateY(ret, angle);
    angle = dist(eng);
    ret = glm::rotateZ(ret, angle);
    return ret;
}

glm::vec3 smallStep(const State& state) {
    std::random_device r;
    std::seed_seq seed{r(), r(), r(), r(), r(), r()};
    std::mt19937_64 eng(seed);
    std::normal_distribution<float> dist(0, 0.05);

    float angle = dist(eng);
    auto ret = glm::rotateX(state.dir, angle);
    angle = dist(eng);
    ret = glm::rotateY(ret, angle);
    angle = dist(eng);
    ret = glm::rotateZ(ret, angle);
    return ret;
}

void makeSticks(const State& state, std::vector<TreeBranch>& sticks, int current_level, const int maxLevel) {

    glm::vec3 d;

    if (current_level == maxLevel) {
        return;
    }

    for (int j = 0; j < 4; j++) {

        d = Step(state);

        State new_state = GenerateNewState(state, d);

        sticks.emplace_back(state.pos, new_state.pos, current_level);

        makeSticks(new_state, sticks, current_level + 1, maxLevel);
    }
//    d = smallStep(state);
//    auto newState = GenerateNewState(state, d);
//    sticks.emplace_back(state.pos, newState.pos, current_level);
//    makeSticks(newState, sticks, current_level + 1, maxLevel);
}

std::vector<TreeBranch> generateTreeSticks(const int maxLevel) {

    State state({0,0,0}, {0,0,1});
    glm::vec3 d;
    std::vector<TreeBranch> sticks;

    int current_level = 0;

    sticks.emplace_back(state.pos, state.dir, current_level);

    makeSticks(GenerateNewState(state, state.dir), sticks, current_level+1, maxLevel);

    return sticks;
}
//
// Created by vika on 24.04.2020.
//

