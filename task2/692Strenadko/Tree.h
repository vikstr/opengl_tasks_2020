
#include <vector>

#include "Application.hpp"
#include "ShaderProgram.hpp"
#include "Mesh.hpp"

#ifndef OPENGL_TASKS_2020_TREE_H
#define OPENGL_TASKS_2020_TREE_H

#define GLM_ENABLE_EXPERIMENTAL

#include <vector>
#include <../../external/GLM/glm/glm.hpp>
#include <../../external/GLM/glm/gtx/rotate_vector.hpp>

template<class Point>
struct SegmentT {
    SegmentT(const Point& p1, const Point& p2)
            : A(p1)
            , B(p2)
    {}

    SegmentT(const SegmentT<Point>& other) = default;

    Point A;
    Point B;
};

struct State {
    State(const glm::vec3& position, const glm::vec3& direction)
            : pos(position)
            , dir(direction)
    {}

    State(const State& other) = default;

    glm::vec3 pos;
    glm::vec3 dir;
};

using Segment3 = SegmentT<glm::vec3>;

template<class Point>
struct TreeBranchT : public SegmentT<Point> {
    TreeBranchT(const Point& p1, const Point& p2, const int lvl)
            : SegmentT<Point>(p1, p2)
            , Level(lvl)
    {}

    TreeBranchT(const SegmentT<Point>& seg, const int lvl)
            : SegmentT<Point>(seg)
            , Level(lvl)
    {}
    int Level;
};

using TreeBranch = TreeBranchT<glm::vec3>;

std::vector<TreeBranch> generateTreeSticks(const int maxLevel=5);
void makeSticks(const State& state, std::vector<TreeBranch>& sticks, int current_level, const int maxLevel);
glm::vec3 Step(const State& state);
State GenerateNewState(const State &state, const glm::vec3 &dir);

#endif //OPENGL_TASKS_2020_MAIN_H
