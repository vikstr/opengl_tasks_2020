#include "get_Mesh.h"
#include "Tree.h"

glm::vec3 getPerpendicular(const glm::vec3& ab) {
    auto p1 = glm::vec3(-ab.y, ab.x, 0);
    if (glm::length(p1) < 1e-5) {
        p1 = glm::vec3(-ab.z, 0, ab.x);
    }
    if (glm::length(p1) < 1e-5) {
        p1 = glm::vec3(0, -ab.z, ab.y);
    }
    assert(glm::length(p1) >= 1e-5);
    return p1;
}

float getRadius(const int level) {
    const float BARK_RADIUS = 0.15f;
    const float INITIAL_RADIUS = 0.10f;
    const float RADIUS_SCALE = 0.4f;

    float radius = level == 0 ? BARK_RADIUS : INITIAL_RADIUS;
    for (int i = 0; i < level - 1; ++i) {
        radius *= RADIUS_SCALE;
    }
    return radius;
}

void CreateCone(
        const glm::vec3& a,
        const glm::vec3& b,
        float r1,
        float r2,
        int N,
        std::vector<glm::vec3>& vertices,
        std::vector<glm::vec3>& normals,
        std::vector<glm::vec2>& tex
) {
    assert(r1 > 0.0 && r2 > 0.0);
    assert(N >= 3);
    auto ab = b - a;

    // перпендикуляры к ab
    auto p1 = getPerpendicular(ab);
    p1 = normalize(p1);

    auto p2 = normalize(cross(ab, p1));

    auto polar_radius = [&](float phi, float r) {
        return p1 * glm::cos(phi) * r + p2 * glm::sin(phi) * r;
    };

    auto addVertex = [&](glm::vec3 v, glm::vec3 normal, glm::vec2 tc) {
        vertices.push_back(v);
        normals.push_back(normal);
        tex.push_back(tc);
    };

    auto addTriangle = [&](glm::vec3 a, glm::vec3 b, glm::vec3 c) {
        auto normal = normalize(cross(a - b, a - c));
        addVertex(a, normal, {0.0f, 0.0f});
        addVertex(b, normal, {0.0f, 0.0f});
        addVertex(c, normal, {0.0f, 0.0f});
        // нормаль смотрит на нас, если вершины треугольника заданы против часовой стрелки
    };

    for (unsigned int i = 0; i < N; ++i) {

        float phi1 = 2.0f * glm::pi<float>() * i / N;
        float phi2 = 2.0f * glm::pi<float>() * (i + 1) / N;

        auto bot1 = a + polar_radius(phi1, r1);
        auto bot2 = a + polar_radius(phi2, r1);

        auto top1 = b + polar_radius(phi1, r2);
        auto top2 = b + polar_radius(phi2, r2);

        // треугольник для основания
        addTriangle(bot2, bot1, a);

        // треугольник для верхушки
        addTriangle(top2, top1, b);

        auto normal = normalize(cross(bot1 - bot2, bot1 - top1));

        // First side triangle
        addVertex(bot1, normal, {i / (float)N, 0.0f});
        addVertex(bot2, normal, {(i + 1) / (float)N, 0.0f});
        addVertex(top1, normal, {i / (float)N, 1.0f});

        // Second side triangle
        addVertex(top2, normal, {(i + 1) / (float)N, 1.0f});
        addVertex(top1, normal, {i / (float)N, 1.0f});
        addVertex(bot2, normal, {(i + 1) / (float)N, 0.0f});
    }
}

void CreateLeaves(
        const glm::vec3& a,
        const glm::vec3& b,
        float size,
        int N,
        std::vector<glm::vec3>& vertices,
        std::vector<glm::vec3>& normals,
        std::vector<glm::vec2>& texcoords
) {
    auto ab = b - a;
    auto nab = normalize(ab);

    // Perpendiculars to ab
    auto p1 = size * normalize(getPerpendicular(ab));
    auto p2 = size * normalize(cross(ab, p1));
    auto p3 = size * normalize(cross(p1, p2));

    auto addVertex = [&](glm::vec3 v, glm::vec2 tc) {
        vertices.push_back(v);
        normals.push_back(nab);
        texcoords.push_back(tc);
    };

    for (int i = 1; i <= N; ++i) {
        auto c = a + (b - a) * (float)(i) / (float)(N);

        // первый квадрат
        addVertex(c - p1 - p2, {0.0, 0.0});
        addVertex(c - p1 + p2, {0.0, 1.0});
        addVertex(c + p1 + p2, {1.0, 1.0});

        addVertex(c + p1 - p2, {1.0, 0.0});
        addVertex(c - p1 - p2, {0.0, 0.0});
        addVertex(c + p1 + p2, {1.0, 1.0});

        // второй квадрат, перпендикулярный первому
        addVertex(c - p1 - p3, {0.0, 0.0});
        addVertex(c - p1 + p3, {0.0, 1.0});
        addVertex(c + p1 + p3, {1.0, 1.0});

        addVertex(c + p1 - p3, {1.0, 0.0});
        addVertex(c - p1 - p3, {0.0, 0.0});
        addVertex(c + p1 + p3, {1.0, 1.0});
    }
}

MeshPtr BuildMesh(const std::vector<glm::vec3>& vertices, const std::vector<glm::vec3>& normals, const std::vector<glm::vec2>& tex) {
    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(tex.size() * sizeof(float) * 2, tex.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);

    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());
    return mesh;
}

std::pair<MeshPtr, MeshPtr> CreateTreeMesh(const int maxLevel, const int leavesStartLevel, const int treeDetalization) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> tex;

    std::vector<glm::vec3> lvertices;
    std::vector<glm::vec3> lnormals;
    std::vector<glm::vec2> ltex;

    std::vector<TreeBranch> sticks = generateTreeSticks(maxLevel);

    for (auto& stick : sticks) {
        CreateCone(
                stick.A,
                stick.B,
                getRadius(stick.Level),
                getRadius(stick.Level + 1),
                treeDetalization,
                vertices,
                normals,
                tex
        );
        if (stick.Level >= leavesStartLevel) {
            CreateLeaves(
                    stick.A,
                    stick.B,
                    0.12,
                    3,
                    lvertices,
                    lnormals,
                    ltex
            );
        }
    }

    return std::make_pair(BuildMesh(vertices, normals, tex), BuildMesh(lvertices, lnormals, ltex));
}
//
// Created by vika on 24.04.2020.
//

