#define GLM_ENABLE_EXPERIMENTAL

#include <vector>
#include "Mesh.hpp"

std::pair<MeshPtr, MeshPtr> CreateTreeMesh(const int maxLevel=5, const int leavesStartLevel=3, const int treeDetalization=100);

void CreateCone(
        const glm::vec3& a,
        const glm::vec3& b,
        float r1,
        float r2,
        int N,
        std::vector<glm::vec3>& vertices,
        std::vector<glm::vec3>& normals
);
//
// Created by vika on 24.04.2020.
//

#ifndef OPENGL_TASKS_2020_GET_MESH_H
#define OPENGL_TASKS_2020_GET_MESH_H

#endif //OPENGL_TASKS_2020_GET_MESH_H
